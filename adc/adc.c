/*
 * adc.c
 *
 * Created: 2/11/2018 2:28:37 PM
 *  Author: ben
 */

#include <asf.h>
#include "adc.h"
#include <inttypes.h>
#include "../usart/config_usart.h"
#include "../ports/port_config.h"

// USART module instance
struct usart_module usart_instance;

// ADC module inst
struct adc_module adc_instance;

volatile uint16_t port_A1_avg_results = 0;
volatile uint16_t port_A2_avg_results = 0;
volatile uint16_t port_A3_avg_results = 0;
volatile uint16_t port_A4_avg_results = 0;

uint16_t adc_result_buffer[ADC_SAMPLES];

void get_A1_adc_avg()
{
	char tmp[8];
	sprintf(tmp, "%u", (int)port_A1_avg_results);
	usart_write_buffer_wait(&usart_instance, (uint8_t *)tmp,  MAX_TX_BUFFER_LENGTH);
}

void get_A2_adc_avg()
{
	char tmp[8];
	sprintf(tmp, "%u", (int)port_A2_avg_results);
	usart_write_buffer_wait(&usart_instance, (uint8_t *)tmp,  MAX_TX_BUFFER_LENGTH);
}

void get_A3_adc_avg()
{
	char tmp[8];
	sprintf(tmp, "%u", (int)port_A3_avg_results);
	usart_write_buffer_wait(&usart_instance, (uint8_t *)tmp,  MAX_TX_BUFFER_LENGTH);
}


void get_A4_adc_avg()
{
	char tmp[8];
	sprintf(tmp, "%u", (int)port_A4_avg_results);
	usart_write_buffer_wait(&usart_instance, (uint8_t *)tmp,  MAX_TX_BUFFER_LENGTH);
}


void sample_analog_ports(void)
{
	adc_read_buffer_job(&adc_instance, adc_result_buffer, ADC_SAMPLES);
}


void adc_complete_callback_port_A1(struct adc_module *const module)
{
	uint32_t accum = 0;
	for (uint8_t i = 0; i < ADC_SAMPLES; i++)
    {
		accum += adc_result_buffer[i];
	}
	port_A1_avg_results = accum/ADC_SAMPLES;

	adc_reset(&adc_instance);
	adc_disable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);

	configure_adc_port_A2();
	configure_adc_callbacks_port_A2();
	adc_enable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);

	adc_read_buffer_job(&adc_instance, adc_result_buffer, ADC_SAMPLES);
}



void adc_complete_callback_port_A2(struct adc_module *const module)
{
	uint32_t accum = 0;
	for (uint8_t i = 0; i < ADC_SAMPLES; i++)
	{
		accum += adc_result_buffer[i];
	}
	port_A2_avg_results = accum/ADC_SAMPLES;

	adc_reset(&adc_instance);
	adc_disable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);

	configure_adc_port_A3();
	configure_adc_callbacks_port_A3();
	adc_enable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);

	adc_read_buffer_job(&adc_instance, adc_result_buffer, ADC_SAMPLES);
}



void adc_complete_callback_port_A3(struct adc_module *const module)
{
	uint32_t accum = 0;
	for (uint8_t i = 0; i < ADC_SAMPLES; i++)
	{
		accum += adc_result_buffer[i];
	}
	port_A3_avg_results = accum/ADC_SAMPLES;

	adc_reset(&adc_instance);
	adc_disable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);

	configure_adc_port_A4();
	configure_adc_callbacks_port_A4();
	adc_enable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);

	adc_read_buffer_job(&adc_instance, adc_result_buffer, ADC_SAMPLES);
}



void adc_complete_callback_port_A4(struct adc_module *const module)
{
	uint32_t accum = 0;
	for (uint8_t i = 0; i < ADC_SAMPLES; i++)
	{
		accum += adc_result_buffer[i];
	}
	port_A4_avg_results = accum/ADC_SAMPLES;

	adc_reset(&adc_instance);
	adc_disable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);

	configure_adc_port_A1();
	configure_adc_callbacks_port_A1();
	adc_enable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);
}



void configure_adc_port_A1(void)
{
	struct adc_config config_adc;
	adc_get_config_defaults(&config_adc);

	config_adc.clock_prescaler = ADC_CLOCK_PRESCALER_DIV8;
	config_adc.reference       = ADC_REFERENCE_AREFA;
	config_adc.positive_input  = ADC_POSITIVE_INPUT_PIN0;
	config_adc.resolution      = ADC_RESOLUTION_12BIT;
	adc_init(&adc_instance, ADC, &config_adc);
	adc_enable(&adc_instance);
}



void configure_adc_callbacks_port_A1(void)
{
	adc_register_callback(&adc_instance, adc_complete_callback_port_A1, ADC_CALLBACK_READ_BUFFER);
	adc_enable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);
}



void configure_adc_port_A2(void)
{
	struct adc_config config_adc;
	adc_get_config_defaults(&config_adc);

	config_adc.clock_prescaler = ADC_CLOCK_PRESCALER_DIV8;
	config_adc.reference       = ADC_REFERENCE_AREFA;
	config_adc.positive_input  = ADC_POSITIVE_INPUT_PIN1;
	config_adc.resolution      = ADC_RESOLUTION_12BIT;

	adc_init(&adc_instance, ADC, &config_adc);
	adc_enable(&adc_instance);
}



void configure_adc_callbacks_port_A2(void)
{
	adc_register_callback(&adc_instance, adc_complete_callback_port_A2, ADC_CALLBACK_READ_BUFFER);
	adc_enable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);
}



void configure_adc_port_A3(void)
{
	struct adc_config config_adc;
	adc_get_config_defaults(&config_adc);

	config_adc.clock_prescaler = ADC_CLOCK_PRESCALER_DIV8;
	config_adc.reference       = ADC_REFERENCE_AREFA;
	config_adc.positive_input  = ADC_POSITIVE_INPUT_PIN2;
	config_adc.resolution      = ADC_RESOLUTION_12BIT;
	adc_init(&adc_instance, ADC, &config_adc);
	adc_enable(&adc_instance);
}



void configure_adc_callbacks_port_A3(void)
{
	adc_register_callback(&adc_instance, adc_complete_callback_port_A3, ADC_CALLBACK_READ_BUFFER);
	adc_enable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);
}



void configure_adc_port_A4(void)
{
	struct adc_config config_adc;
	adc_get_config_defaults(&config_adc);

	config_adc.clock_prescaler = ADC_CLOCK_PRESCALER_DIV8;
	config_adc.reference       = ADC_REFERENCE_AREFA;
	config_adc.positive_input  = ADC_POSITIVE_INPUT_PIN3;
	config_adc.resolution      = ADC_RESOLUTION_12BIT;
	adc_init(&adc_instance, ADC, &config_adc);
	adc_enable(&adc_instance);
}


void configure_adc_callbacks_port_A4(void)
{
	adc_register_callback(&adc_instance, adc_complete_callback_port_A4, ADC_CALLBACK_READ_BUFFER);
	adc_enable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);
}
