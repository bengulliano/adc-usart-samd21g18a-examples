/*
 * adc.h
 *
 * Created: 2/11/2018 2:28:17 PM
 *  Author: ben
 */ 


#ifndef ADC_H_
#define ADC_H_

#define ADC_SAMPLES 64

void sample_analog_ports(void);

void configure_adc_port_A1(void);
void configure_adc_port_A2(void);
void configure_adc_port_A3(void);
void configure_adc_port_A4(void);

void configure_adc_callbacks_port_A1(void);
void configure_adc_callbacks_port_A2(void);
void configure_adc_callbacks_port_A3(void);
void configure_adc_callbacks_port_A4(void);

void adc_complete_callback_port_A1(struct adc_module *const module);
void adc_complete_callback_port_A2(struct adc_module *const module);
void adc_complete_callback_port_A3(struct adc_module *const module);
void adc_complete_callback_port_A4(struct adc_module *const module);


void get_A1_adc_avg(void);
void get_A2_adc_avg(void);
void get_A3_adc_avg(void);
void get_A4_adc_avg(void);

#endif /* ADC_H_ */