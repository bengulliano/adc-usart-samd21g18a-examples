/*
 * common.c
 *
 * Created: 2/11/2018 6:28:05 PM
 *  Author: ben
 */

#include "common.h"


// converts unsigned int16 num to string
void tostring(uint8_t *str, uint16_t num)
{
	int i, rem, len = 0, n;
	n = num;
	while (n != 0)
	{
		len++;
		n /= 10;
	}

	for (i = 0; i < len; i++) {
		rem = num % 10;
		num = num / 10;
		str[len - (i + 1)] =  rem + '0';
	}
	str[len] = '\0';
}
