/*
 * common.h
 *
 * Created: 2/11/2018 6:28:24 PM
 *  Author: ben
 */ 


#ifndef COMMON_H_
#define COMMON_H_

#include <inttypes.h>

void tostring(uint8_t *str, uint16_t num);


#endif /* COMMON_H_ */