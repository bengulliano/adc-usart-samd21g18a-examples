/*
 * partCounter.c
 *
 * Created: 3/3/2018 10:36:03 AM
 *  Author: ben
 *
 *
 */


#include <asf.h>

#include "partCounter.h"
#include "../ports/port_config.h"

volatile uint16_t D1_event_count = 0; //increment this when a state change event is detected on D1, we are interested in high states
volatile uint16_t D2_event_count = 0; //increment this when a state change event is detected on D2, we are interested in high states
volatile uint16_t D3_event_count = 0; //increment this when a state change event is detected on D3, we are interested in high states
volatile uint16_t D4_event_count = 0; //increment this when a state change event is detected on D4, we are interested in high states

volatile bool readingCounter1 = false;
volatile bool readingCounter2 = false;
volatile bool readingCounter3 = false;
volatile bool readingCounter4 = false;


void resetCounter1() {
	 D1_event_count = 0;
}

void resetCounter2() {
	D2_event_count = 0;
}

void resetCounter3() {
	D3_event_count = 0;
}

void resetCounter4() {
	D4_event_count = 0;
}

void inCounter1ReadMode(bool arg) {
	readingCounter1 = arg;
}


void inCounter2ReadMode(bool arg) {
	readingCounter2 = arg;
}

void inCounter3ReadMode(bool arg) {
	readingCounter3 = arg;
}

void inCounter4ReadMode(bool arg) {
	readingCounter4 = arg;
}



uint32_t get_D1_event_Count() {
	uint16_t tmp = D1_event_count;
	return tmp;
}

uint32_t get_D2_event_Count() {
	uint16_t tmp = D2_event_count;
	return tmp;
}

uint32_t get_D3_event_Count() {
	uint16_t tmp = D3_event_count;
	return tmp;
}

uint32_t get_D4_event_Count() {
	uint16_t tmp = D4_event_count;
	return tmp;
}


void configure_D1_extint_channel() {
	struct extint_chan_conf config_extint_chan;
	extint_chan_get_config_defaults(&config_extint_chan);
	config_extint_chan.gpio_pin           = PIN_PB10A_EIC_EXTINT10;
	config_extint_chan.gpio_pin_mux       = MUX_PB10A_EIC_EXTINT10;
	config_extint_chan.gpio_pin_pull      = EXTINT_PULL_DOWN;
	config_extint_chan.detection_criteria = EXTINT_DETECT_BOTH;
	config_extint_chan.filter_input_signal = true;
	extint_chan_set_config(10, &config_extint_chan);
}


void configure_D1_extint_callbacks() {
	extint_register_callback(D1_extint_detection_callback, 10, EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(10, EXTINT_CALLBACK_TYPE_DETECT);
}

void D1_extint_detection_callback() {
	bool pin_state = PORT->Group[1].IN.bit.IN & (1 << 10); //check if it is high, is bit 10 on this register 1?
	while (readingCounter1){}
	if (pin_state == 1) {		
		D1_event_count++;
		if (D1_event_count == 100000)
		D1_event_count = 1;
	}
}



void configure_D2_extint_channel() {
	struct extint_chan_conf config_extint_chan;
	extint_chan_get_config_defaults(&config_extint_chan);
	config_extint_chan.gpio_pin           = PIN_PA19A_EIC_EXTINT3;
	config_extint_chan.gpio_pin_mux       = MUX_PA19A_EIC_EXTINT3;
	config_extint_chan.gpio_pin_pull      = EXTINT_PULL_DOWN;
	config_extint_chan.detection_criteria = EXTINT_DETECT_BOTH;
	config_extint_chan.filter_input_signal = true;
	extint_chan_set_config(3, &config_extint_chan);
}


void configure_D2_extint_callbacks() {
	extint_register_callback(D2_extint_detection_callback,	3, 	EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(3, EXTINT_CALLBACK_TYPE_DETECT);
}

void D2_extint_detection_callback() {
	bool pin_state = PORT->Group[0].IN.bit.IN & (1 << 19); //check if it is high, is bit 24 on this register 1?
	while (readingCounter2){}
	if (pin_state == 1)	{
		D2_event_count++;
		if (D2_event_count == 100000)
			D2_event_count = 1;
	}
}



void configure_D3_extint_channel() {
	struct extint_chan_conf config_extint_chan;
	extint_chan_get_config_defaults(&config_extint_chan);
	config_extint_chan.gpio_pin           = PIN_PA20A_EIC_EXTINT4;
	config_extint_chan.gpio_pin_mux       = MUX_PA20A_EIC_EXTINT4;
	config_extint_chan.gpio_pin_pull      = EXTINT_PULL_DOWN;
	config_extint_chan.detection_criteria = EXTINT_DETECT_BOTH;
	config_extint_chan.filter_input_signal = true;
	extint_chan_set_config(4, &config_extint_chan);
}


void configure_D3_extint_callbacks() {
	extint_register_callback(D3_extint_detection_callback,	4, 	EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(4, EXTINT_CALLBACK_TYPE_DETECT);
}

void D3_extint_detection_callback() {
	bool pin_state = PORT->Group[0].IN.bit.IN & (1 << 20);  //check if it is high, is bit 20 on this register 1?
	while (readingCounter3){}
	if (pin_state == 1)	{
		D3_event_count++;
		if (D3_event_count == 100000)
			D3_event_count = 1;
	}
}



void configure_D4_extint_channel() {
	struct extint_chan_conf config_extint_chan;
	extint_chan_get_config_defaults(&config_extint_chan);
	config_extint_chan.gpio_pin           = PIN_PA22A_EIC_EXTINT6;
	config_extint_chan.gpio_pin_mux       = MUX_PA22A_EIC_EXTINT6;
	config_extint_chan.gpio_pin_pull      = EXTINT_PULL_DOWN;
	config_extint_chan.detection_criteria = EXTINT_DETECT_BOTH;
	config_extint_chan.filter_input_signal = true;
	extint_chan_set_config(6, &config_extint_chan);
}


void configure_D4_extint_callbacks() {
	extint_register_callback(D4_extint_detection_callback,	6, 	EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(6, EXTINT_CALLBACK_TYPE_DETECT);
}

void D4_extint_detection_callback() {
	bool pin_state = PORT->Group[0].IN.bit.IN & (1 << 22);		//check if it is high, is bit 22 on this register 1?
	while (readingCounter4){}
	if (pin_state == 1)	{
		D4_event_count++;
		if (D4_event_count == 100000)
			D4_event_count = 1;
	}
		port_pin_set_output_level(D4, 0);
}
