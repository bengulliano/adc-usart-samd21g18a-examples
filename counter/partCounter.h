/*
 * partCounter.h
 *
 * Created: 3/3/2018 10:37:52 AM
 *  Author: ben
 */ 


#ifndef PARTCOUNTER_H_
#define PARTCOUNTER_H_


void configure_D1_extint_channel(void);
void configure_D1_extint_callbacks(void);
void D1_extint_detection_callback(void);

void configure_D2_extint_channel(void);
void configure_D2_extint_callbacks(void);
void D2_extint_detection_callback(void);

void configure_D3_extint_channel(void);
void configure_D3_extint_callbacks(void);
void D3_extint_detection_callback(void);

void configure_D4_extint_channel(void);
void configure_D4_extint_callbacks(void);
void D4_extint_detection_callback(void);

uint32_t get_D1_event_Count(void); 
uint32_t get_D2_event_Count(void);
uint32_t get_D3_event_Count(void);
uint32_t get_D4_event_Count(void);

void inCounter1ReadMode(bool arg);
void inCounter2ReadMode(bool arg);
void inCounter3ReadMode(bool arg);
void inCounter4ReadMode(bool arg);

void resetCounter1(void);
void resetCounter2(void);
void resetCounter3(void);
void resetCounter4(void);

#endif /* PARTCOUNTER_H_ */