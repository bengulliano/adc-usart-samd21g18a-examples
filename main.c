/*
 * main.c
 *
 * Created: 1/3/2018 09:22:55 AM
 *  Author: ben
 *
 *
 */
#include <asf.h>
#include "main.h"

// USART module instance
struct usart_module usart_instance;

// TC modules
struct tc_module tc_instance;
struct tc_module usart_reset_timer;

volatile bool REQUEST_REC_ON_USART = false;
volatile bool REC_MCP_HANDSHAKE = false;
volatile int handShakesMissed = 0;
uint8_t rx_received[10];
volatile unsigned rx_ptr = 0;
volatile bool message_complete = false;

int main (void)
{
	system_clock_init();
	system_init();
	board_init();
	ioport_init();
	system_interrupt_enable_global();

	config_digital_ports();
	config_analog_ports();

	configure_tc();
	configure_tc_callbacks();

	configure_usart();
	configure_usart_callbacks();

	configure_adc_port_A1();
	configure_adc_callbacks_port_A1();

	configure_D1_extint_channel();
	configure_D1_extint_callbacks();
	D1_extint_detection_callback();

	configure_D2_extint_channel();
	configure_D2_extint_callbacks();
	D2_extint_detection_callback();

	configure_D3_extint_channel();
	configure_D3_extint_callbacks();
	D3_extint_detection_callback();

	configure_D4_extint_channel();
	configure_D4_extint_callbacks();
	D4_extint_detection_callback();

	//uint8_t hello_string [] = "\nconnected !\r\n";
	//usart_write_buffer_wait(&usart_instance, hello_string, sizeof(hello_string));

	while (true) {
		usart_read_buffer_job(&usart_instance, (uint8_t *)Rx_buffer, MAX_RX_BUFFER_LENGTH);
		if(message_complete) {
			rx_ptr = 0;
			parseMessage();
			message_complete = false;
		}
	}
}


void parseMessage() {
	usart_write_buffer_wait(&usart_instance, (uint8_t *)rx_received, sizeof(rx_received));
	memset(rx_received, 0, sizeof(rx_received));
	usart_write_buffer_wait(&usart_instance, (uint8_t *)Rx_buffer, MAX_RX_BUFFER_LENGTH);
}


 void copyFromBuffer()
{
	for (int i=0; i< MAX_RX_BUFFER_LENGTH; i++) {
		if (Rx_buffer[i] != (int)('\n')) {
			rx_received[rx_ptr] = (uint8_t)Rx_buffer[i];
			rx_ptr++;
		} else {
			message_complete = true;
		}
	}

}


void configure_tc(void)
{
	struct 	tc_config config_tc;
	tc_get_config_defaults(&config_tc);
	config_tc.counter_size = TC_COUNTER_SIZE_16BIT;
	config_tc.clock_source = GCLK_GENERATOR_0;
	config_tc.clock_prescaler = TC_CLOCK_PRESCALER_DIV64;
	tc_init(&tc_instance, ANALOG_SCAN_TC_TIMER_MODULE, &config_tc);
	tc_enable(&tc_instance);
}

void tc_callback_to_sample_adc(struct tc_module * const module_inst)
{
	port_pin_toggle_output_level(LED0_PIN);
	sample_analog_ports();
}


void configure_tc_callbacks(void)
{
	tc_register_callback(&tc_instance, tc_callback_to_sample_adc, TC_CALLBACK_OVERFLOW);
	tc_enable_callback(&tc_instance, TC_CALLBACK_OVERFLOW);
}



void configure_usart_reset_timer(void)
{
	struct 	tc_config config_tc;
	tc_get_config_defaults(&config_tc);
	config_tc.counter_size = TC_COUNTER_SIZE_16BIT;
	config_tc.clock_source = GCLK_GENERATOR_0;
	config_tc.clock_prescaler = TC_CLOCK_PRESCALER_DIV256;
	tc_init(&usart_reset_timer, USART_RESET_TC_TIMER_MODULE, &config_tc);
	tc_enable(&usart_reset_timer);
}

void tc_callback_to_reset_usart(struct tc_module * const module_inst)
{
	if (REC_MCP_HANDSHAKE == true) {
		REC_MCP_HANDSHAKE = false;
		handShakesMissed = 0;
	} else {
		handShakesMissed++;
		if (handShakesMissed == 3) {
			REQUEST_REC_ON_USART = false;
			usart_reset(&usart_instance);
			configure_usart();
			configure_usart_callbacks();
			tc_reset(&usart_reset_timer);
		}
	}
}


void configure_usart_reset_tc_callbacks(void)
{
	tc_register_callback(&usart_reset_timer, tc_callback_to_reset_usart, TC_CALLBACK_OVERFLOW);
	tc_enable_callback(&usart_reset_timer, TC_CALLBACK_OVERFLOW);
}
