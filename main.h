/*
 * main.h
 *
 * Created: 2/7/2018 3:41:13 PM
 *  Author: Ben
 */ 


#ifndef MMAIN_H_
#define MAIN_H_

#include <delay.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include "adc/adc.h"
#include "counter/partCounter.h"
#include "ports/port_config.h"
#include "usart/config_usart.h"

#define F_CPU 8000000UL

#define TC_PW_MODULE TC5
#define ANALOG_SCAN_TC_TIMER_MODULE TC4
#define USART_RESET_TC_TIMER_MODULE TC3

void configure_tc_callbacks(void);
void configure_tc(void);
void tc_callback_to_sample_adc(struct tc_module * const module_inst);
void parseMessage(void);
void configure_usart_reset_timer(void);
void configure_usart_reset_tc_callbacks(void);
void tc_callback_to_reset_usart(struct tc_module * const module_inst);



#endif /* MAIN_H_ */