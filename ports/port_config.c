/*
 * port_config.c
 *
 * Created: 2/10/2018 3:24:24 PM
 *  Author: ben
 */ 

#include <asf.h>
#include "port_config.h"


void config_digital_ports(void)
{
	struct port_config pin_conf;
	port_get_config_defaults(&pin_conf);
	pin_conf.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(PIN_PA13, &pin_conf);
	port_pin_set_config(USART_TX, &pin_conf);

	port_pin_set_output_level(PIN_PA13, LED0_INACTIVE);

	struct port_config pin_conf1;
	port_get_config_defaults(&pin_conf1);
	pin_conf1.direction = PORT_PIN_DIR_INPUT;
	pin_conf1.input_pull = PORT_PIN_PULL_DOWN;

	port_pin_set_config(D1, &pin_conf1);
	port_pin_set_config(D2, &pin_conf1);
	port_pin_set_config(D3, &pin_conf1);
	port_pin_set_config(D4, &pin_conf1);
	
	struct port_config pin_conf2;
	port_get_config_defaults(&pin_conf2);
	pin_conf2.direction = PORT_PIN_DIR_INPUT;
	port_pin_set_config(USART_RX, &pin_conf2);	

}



void config_analog_ports(void)
{
	struct port_config pin_conf;
	port_get_config_defaults(&pin_conf);	
	pin_conf.direction = PORT_PIN_DIR_INPUT;
	pin_conf.input_pull = PORT_PIN_PULL_DOWN;
	
	port_pin_set_config(A1, &pin_conf);
	port_pin_set_config(A2, &pin_conf);
	port_pin_set_config(A3, &pin_conf);
	port_pin_set_config(A4, &pin_conf);		
}