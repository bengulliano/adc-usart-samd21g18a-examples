/*
 * port_config.h
 *
 * Created: 2/10/2018 3:24:03 PM
 *  Author: Ben
 */ 


#ifndef PORT_CONFIG_H_
#define PORT_CONFIG_H_

//	Digital Ports
#define D1  PIN_PB10
#define D2  PIN_PA19
#define D3  PIN_PA20
#define D4  PIN_PA22

//	Analog Ports
#define A1 PIN_PA02
#define A2 PIN_PA03
#define A3 PIN_PB08
#define A4 PIN_PB09

// Blue LED
#define LED0_PIN PIN_PA13
#define LED0_INACTIVE 0

#define USART_TX PIN_PB22
#define USART_RX PIN_PB23

void config_digital_ports(void);
void config_analog_ports(void);

#endif /* PORT_CONFIG_H_ */