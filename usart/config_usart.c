/*
 * usart_functions.c
 *
 * Created: 2/10/2018 3:09:17 PM
 *  Author: ben
 */ 

#include <asf.h>
#include "config_usart.h" 

#define LED_0_PIN PIN_PA13

// USART module instance
struct usart_module usart_instance;

void configure_usart()
{
	struct usart_config config_usart;
	usart_get_config_defaults(&config_usart);
	config_usart.baudrate  =  57600;
	config_usart.mux_setting = USART_RX_3_TX_2_XCK_3;
	config_usart.transmitter_enable = true;
	config_usart.receiver_enable = true;
	config_usart.transfer_mode = USART_TRANSFER_ASYNCHRONOUSLY;
	
	config_usart.pinmux_pad2 = PINMUX_PB22D_SERCOM5_PAD2;
	config_usart.pinmux_pad3 = PINMUX_PB23D_SERCOM5_PAD3;
	while(usart_init(&usart_instance, SERCOM5, &config_usart) != STATUS_OK) {}
	
	usart_enable(&usart_instance);
}

void usart_write_callback(struct usart_module * const usart_module)
{
	port_pin_toggle_output_level(LED_0_PIN);	
}


void usart_read_callback(struct usart_module * const usart_module)
{
	 copyFromBuffer();
}


void configure_usart_callbacks(void)
{
	usart_register_callback(&usart_instance, usart_write_callback, USART_CALLBACK_BUFFER_TRANSMITTED);
	usart_register_callback(&usart_instance, usart_read_callback, USART_CALLBACK_BUFFER_RECEIVED);
	usart_enable_callback(&usart_instance, 	USART_CALLBACK_BUFFER_TRANSMITTED);
	usart_enable_callback(&usart_instance, USART_CALLBACK_BUFFER_RECEIVED);
}




