/*
 * usart_f.h
 *
 * Created: 2/10/2018 3:10:15 PM
 *  Author: ben
 */ 


#ifndef USART_F_H_
#define USART_F_H_

#define MAX_RX_BUFFER_LENGTH 5
#define MAX_TX_BUFFER_LENGTH 5 
#define MAX_TX_BUFFER_LENGTH_STRING 124 
#define MAX_ADC_RESULT_LENGTH 12

volatile uint8_t Rx_buffer[MAX_RX_BUFFER_LENGTH];

void copyFromBuffer(void);
void configure_usart(void);
void tc_callback_to_usart_write(struct tc_module *const module_inst);
void configure_usart_callbacks(void);
void usart_read_callback(struct usart_module * const usart_module);
void usart_write_callback(struct usart_module * const usart_module);
void reset_usart(void);

#endif /* USART_F_H_ */